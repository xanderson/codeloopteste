// import { ToastService } from '../../providers/util/toast.service';
// import { AlertService } from '../../providers/util/alert.service';
import { Component } from '@angular/core';
// import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AlunoProvider, Aluno } from '../../providers/alunos/alunos'

@IonicPage()
@Component({
  selector: 'page-dados-aluno',
  templateUrl: 'dados-aluno.html',
})

export class DadosAlunoPage {

  model: Aluno;

  edicao = false;

  serie: any;

  series = ['1° ano', '2° ano', '3° ano', '4° ano', '5° ano', '6° ano', '7° ano', '8° ano', '9° ano'];

  user = {
    name: 'Marty Mcfly'
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private toast: ToastController, private alunoProvider: AlunoProvider) {
    this.model = new Aluno();

    if (this.navParams.data.id) {
      this.alunoProvider.get(this.navParams.data.id)
        .then((result: any) => {
          this.model = result;
        })
    }
  }

  salvar() {
    this.salvarAluno()
      .then(() => {
        this.toast.create({ message: 'Aluno salvo.', duration: 3000, position: 'botton' }).present();
        this.navCtrl.pop();
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao salvar aluno.', duration: 3000, position: 'botton' }).present();
      });
  }

  private salvarAluno() {
    if (this.model.id) {
      return this.alunoProvider.update(this.model);
    } else {
      return this.alunoProvider.insert(this.model);
    }
  }

}

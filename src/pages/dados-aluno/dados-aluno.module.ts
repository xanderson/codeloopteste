import { DadosAlunoPage } from './dados-aluno';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    DadosAlunoPage,
  ],
  imports: [
    IonicPageModule.forChild(DadosAlunoPage),
  ],
  exports: [
    DadosAlunoPage
  ],
  providers: [
    // ToastService,
    // AlertService
  ]
})

export class DadosAlunoPageModule { }

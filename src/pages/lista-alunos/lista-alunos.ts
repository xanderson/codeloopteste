import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, ToastController } from 'ionic-angular';
import { DadosAlunoPage } from '../dados-aluno/dados-aluno';
import { AlunoProvider, Aluno } from '../../providers/alunos/alunos'

@IonicPage()
@Component({
  selector: 'lista-alunos',
  templateUrl: 'lista-alunos.html',
})
export class ListaAlunosPage {

  alunos: any[] = [];
  
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, private toast: ToastController, private alunoProvider: AlunoProvider) { 
    this.listarAlunos();
  }

  // items = [
  //   {
  //     title: 'Item 1',
  //   },
  //   {
  //     title: 'Item 2',
  //   },
  //   {
  //     title: 'Item 3',
  //   },
  // ];

  listarAlunos() {
    this.alunoProvider.getAll()
      .then((result: any[]) => {
        this.alunos = result;
        console.log(this.alunos);
      });
  }

  deleteAluno(aluno: Aluno) {
    this.alunoProvider.remove(aluno.id)
      .then(() => {
        // Removendo do array de alunos
        var index = this.alunos.indexOf(aluno);
        this.alunos.splice(index, 1);
        this.toast.create({ message: 'Aluno removido.', duration: 3000, position: 'botton' }).present();
      })
  }

  deleteItem(list, index) {
    list.splice(index,1);
  }

  showConfirm(list, index) {
    const confirm = this.alertCtrl.create({
      title: 'Certeza de que deseja excluir?',
      message: 'O aluno será excluído permanentemente do sistema',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {}
        },
        {
          text: 'Excluir',
          handler: () => {
            this.deleteItem(list, index);
            this.deleteAluno(list[index]);
          }
        }
      ]
    });
    confirm.present();
  }
  addAluno() {
    this.navCtrl.push(DadosAlunoPage);
  }

  editarAluno(id: number) {
    this.navCtrl.push(DadosAlunoPage, { id: id });
  }

}

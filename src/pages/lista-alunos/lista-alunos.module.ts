import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaAlunosPage } from './lista-alunos';
import { AlunoProvider } from '../../providers/alunos/alunos'

@NgModule({
  declarations: [
    ListaAlunosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaAlunosPage),
  ],
  exports: [
    ListaAlunosPage
  ],
  providers:[
    AlunoProvider
  ]
})
export class ListaAlunosPageModule {}

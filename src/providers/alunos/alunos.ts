import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class AlunoProvider {

    constructor(private dbProvider: DatabaseProvider) { }

    public insert(aluno: Aluno) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = `insert into alunos (nome_aluno, nascimento, serie, cep, rua, numero, complem, bairro, cidade, estado, nome_mae, cpf_mae, data_pagamento) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
                let data = [aluno.nome_aluno, aluno.nascimento, aluno.serie, aluno.cep, aluno.rua, aluno.numero, aluno.complem, aluno.bairro, aluno.cidade, aluno.estado, aluno.nome_mae, aluno.cpf_mae, aluno.data_pagamento];

                return db.executeSql(sql, data)
                    .catch((e) => console.error(e));
            })
            .catch((e) => console.error(e));
    }

    public update(aluno: Aluno) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'update alunos set nome_aluno = ?, nascimento = ?, serie = ?, cep = ?, rua = ?, numero = ?, complem = ?, bairro = ?, cidade = ?, estado = ?, nome_mae = ?, cpf_mae = ?, data_pagamento = ? where id = ?';
                let data = [aluno.nome_aluno, aluno.nascimento, aluno.serie, aluno.cep, aluno.rua, aluno.numero, aluno.complem, aluno.bairro, aluno.cidade, aluno.estado, aluno.nome_mae, aluno.cpf_mae, aluno.data_pagamento, aluno.id];

                return db.executeSql(sql, data)
                    .catch((e) => console.error(e));
            })
            .catch((e) => console.error(e));
    }

    public remove(id: number) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'delete from alunos where id = ?';
                let data = [id];

                return db.executeSql(sql, data)
                    .catch((e) => console.error(e));
            })
            .catch((e) => console.error(e));
    }

    public get(id: number) {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'select * from alunos where id = ?';
                let data = [id];

                return db.executeSql(sql, data)
                    .then((data: any) => {
                        if (data.rows.length > 0) {
                            let item = data.rows.item(0);
                            let aluno = new Aluno();
                            aluno.id = item.id;
                            aluno.nome_aluno = item.nome_aluno;
                            aluno.nascimento = item.nascimento;
                            aluno.serie = item.serie;
                            aluno.cep = item.cep;
                            aluno.rua = item.rua;
                            aluno.numero = item.numero;
                            aluno.complem = item.complem;
                            aluno.bairro = item.bairro;
                            aluno.cidade = item.cidade;
                            aluno.estado = item.estado;
                            aluno.nome_mae = item.nome_mae;
                            aluno.cpf_mae = item.cpf_mae;
                            aluno.data_pagamento = item.data_pagamento;

                            return aluno;
                        }

                        return null;
                    })
                    .catch((e) => console.error(e));
            })
            .catch((e) => console.error(e));
    }

    public getAll() {
        return this.dbProvider.getDB()
            .then((db: SQLiteObject) => {
                let sql = 'SELECT * FROM alunos';
                // var data: any[];

                return db.executeSql(sql)
                    .then((data: any) => {
                        if (data.rows.length > 0) {
                            let alunos: any[] = [];
                            for (var i = 0; i < data.rows.length; i++) {
                                var aluno = data.rows.item(i);
                                alunos.push(aluno);
                            }
                            return alunos;
                        } else {
                            return [];
                        }
                    })
                    .catch((e) => console.error(e));
            })
            .catch((e) => console.error(e));
    }
}

export class Aluno {
    id: number;
    nome_aluno: string;
    nascimento: Date;
    serie: string;
    cep: string;
    rua: string;
    numero: string;
    complem: string;
    bairro: string;
    cidade: string;
    estado: string;
    nome_mae: string;
    cpf_mae: string;
    data_pagamento: Date;
}
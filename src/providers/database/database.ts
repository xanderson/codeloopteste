import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
  
export class DatabaseProvider {
  
  constructor(private sqlite: SQLite) { }

  /**
   * Cria um banco caso não exista ou pega um banco existente com o nome no parametro
   */
  public getDB() {
    return this.sqlite.create({
      name: 'teste.db',
      location: 'default'
    });
  }

  /**
   * Cria a estrutura inicial do banco de dados
   */
  public createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {

        // Criando as tabelas
        this.createTables(db);

        // Inserindo dados padrão
        this.insertDefaultItems(db);

      })
      .catch(e => console.log(e));
  }

  /**
   * Criando as tabelas no banco de dados
   * @param db
   */
  private createTables(db: SQLiteObject) {
    // Criando as tabelas
    db.sqlBatch([
      [`CREATE TABLE IF NOT EXISTS usuarios (
            id integer primary key AUTOINCREMENT NOT NULL, 
            nome TEXT, 
            senha TEXT)
      `],
      [`CREATE TABLE IF NOT EXISTS alunos (
            id integer primary key AUTOINCREMENT NOT NULL, 
            nome_aluno TEXT, 
            nascimento DATE, 
            serie TEXT, 
            cep TEXT, 
            rua TEXT, 
            numero TEXT,
            complem TEXT, 
            bairro TEXT, 
            cidade TEXT, 
            estado TEXT, 
            nome_mae TEXT, 
            cpf_mae TEXT, 
            data_pagamento DATE, 
      `]
    ])
      .then(() => console.log('Tabelas criadas'))
      .catch(e => console.error('Erro ao criar as tabelas', e));
  }

  /**
   * Incluindo os dados padrões
   * @param db
   */
  private insertDefaultItems(db: SQLiteObject) {
    db.executeSql('select COUNT(id) as qtd from categories')
      .then((data: any) => {
        //Se não existe nenhum registro
        if (data.rows.item(0).qtd == 0) {

          // Criando as tabelas
          db.sqlBatch([
            ['insert into alunos (nome_aluno, nascimento, serie, cep, rua, numero, complem, bairro, cidade, estado, nome_mae, cpf_mae, data_pagamento) values (?,?,?,?,?,?,?,?,?,?,?,?,?)', ['Hambúrgueres', '2018-09-07','1° ano', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '2018-09-20']],
            ['insert into alunos (nome_aluno, nascimento, serie, cep, rua, numero, complem, bairro, cidade, estado, nome_mae, cpf_mae, data_pagamento) values (?,?,?,?,?,?,?,?,?,?,?,?,?)', ['Bebidas', '2018-09-07','1° ano', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '2018-09-20']],
            ['insert into alunos (nome_aluno, nascimento, serie, cep, rua, numero, complem, bairro, cidade, estado, nome_mae, cpf_mae, data_pagamento) values (?,?,?,?,?,?,?,?,?,?,?,?,?)', ['Sobremesas', '2018-09-07','1° ano', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '13144590', '2018-09-20']]
          ])
            .then(() => console.log('Dados padrões incluídos'))
            .catch(e => console.error('Erro ao incluir dados padrões', e));

        }
      })
      .catch(e => console.error('Erro ao consultar a qtd de categorias', e));
  }
}
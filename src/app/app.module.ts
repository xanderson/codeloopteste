import { ErrorHandler, NgModule, LOCALE_ID } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule} from "ionic-angular";
import {BrowserModule} from '@angular/platform-browser';
// import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule} from '@ionic/storage';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard';

import {MyApp} from "./app.component";

import { SQLite } from '@ionic-native/sqlite'
import { DatabaseProvider } from '../providers/database/database';
import { AlunoProvider } from '../providers/alunos/alunos';

import { DadosAlunoPage } from '../pages/dados-aluno/dados-aluno';
import { ListaAlunosPage} from '../pages/lista-alunos/lista-alunos';
import {LoginPage} from "../pages/login/login";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DadosAlunoPage,
    ListaAlunosPage
  ],
  imports: [
    BrowserModule,
    // HttpClientModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot({
      name: '__ionic3_start_theme',
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DadosAlunoPage,
    ListaAlunosPage
  ],
  providers: [
    StatusBar,
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SplashScreen,
    Keyboard,
    // ActivityService,
    SQLite,
    DatabaseProvider,
    AlunoProvider,
  ]
})

export class AppModule {
}

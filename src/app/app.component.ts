import { Component, ViewChild } from "@angular/core";
import { Platform, Nav } from "ionic-angular";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

// import { HomePage } from "../pages/home/home";
import { ListaAlunosPage } from "../pages/lista-alunos/lista-alunos";
import { LoginPage } from "../pages/login/login";
import { DatabaseProvider } from '../providers/database/database'

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  appMenuItems: Array<MenuItem>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public keyboard: Keyboard, 
    public dbProvider: DatabaseProvider
  ) {
    this.splashScreen.show();

    this.initializeApp();

    this.appMenuItems = [
      { title: 'Alunos', component: ListaAlunosPage, icon: 'search'},
      // {title: 'Local Weather', component: LocalWeatherPage, icon: 'partly-sunny'},
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.

      //*** Control Status Bar
      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);

      //*** Control Keyboard
      this.keyboard.disableScroll(true);
      this.dbProvider.createDatabase()
        .then(() => {
          // fechando a SplashScreen somente quando o banco for criado
          this.openInitialPage(this.splashScreen);
        })
        .catch(() => {
          // ou se houver erro na criação do banco
          this.openInitialPage(this.splashScreen);
        });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  private openInitialPage(splashScreen: SplashScreen) {
    splashScreen.hide();
    this.rootPage = LoginPage;
  }

  logout() {
    this.nav.setRoot(LoginPage);
  }

}
